import React, {Component} from 'react'
import { Row, Col, FormGroup, Label, Input, Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import Select from 'react-select';

class TambahDataNonBdki extends Component {
    // constructor(props){
    //     super(props);
    // }

    toggle = (e) => {
        this.props.toggle  && this.props.toggle(e)
    }

    render(){
        
        return(
            <div>
                <Row>
                    <Modal isOpen={this.props.bdkiModal} size="lg" style={{ padding: '10px', width: '100%' }} >
                        <ModalHeader toggle={(e) => this.toggle(e)}>Entri Non BDKI</ModalHeader>
                        <ModalBody>
                            <Col md="12">
                                <FormGroup>
                                    <Row>
                                        <Col md="12">
                                            <Label>Identitas</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="3" xs="12">
                                            <Label>1. Nama Lengkap : </Label>
                                        </Col>
                                        <Col md="4" xs="12">
                                            <Input type="text" id="text-input" name="bdkiNama" onChange={this.bdkiChange} 
                                            // value={this.state.bdkiNama} 
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Label>2. Tanggal Lahir : </Label>
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Input type="date" id="text-input" name="bdkiTgl" onChange={this.bdkiChange} 
                                            // value={this.state.bdkiTgl} 
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Label>3. Jenis Kelamin :</Label>
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Select 
                                            // options={jenis_kelamin} 
                                            name="bdkiJk" onChange={this.handleJk} 
                                            // value={this.state.bdkiJk} 
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Label>4. NIK :</Label>
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Input type="number" id="text-input" name="bdkiNIK" onChange={this.bdkiChange} 
                                            // value={this.state.bdkiNIK} 
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Label>5. Jenis Peserta :</Label>
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Input type="read-only" id="text-input" name="bdkiJp" onChange={this.bdkiChange} 
                                            // value={this.state.bdkiJp} 
                                            placeholder="Peserta Baru" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="12" style={{ marginTop: '10px' }}>
                                            <Label>Wilayah Alamat</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Label>Provinsi : </Label>
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Select 
                                            // options={this.state.opt_prov} onChange={this.handleProv} 
                                            // value={this.state.provinsi} 
                                            isClearable />
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Label>Kabupaten/Kota : </Label>
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Select 
                                            // options={this.state.opt_kab} onChange={this.handleKab} 
                                            // value={this.state.kabupaten} isDisabled={this.state.dis_kab} 
                                            isClearable />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Label>Kecamatan : </Label>
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Select 
                                            // options={this.state.opt_kec} 
                                            onChange={this.handleKec} 
                                            // value={this.state.kecamatan} isDisabled={this.state.dis_kec} 
                                            isClearable />
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Label>Desa/Kelurahan : </Label>
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Select 
                                            // options={this.state.opt_des} 
                                            // onChange={this.handleDes} value={this.state.desa} isDisabled={this.state.dis_des} 
                                            isClearable />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Label>Dusun/RW : </Label>
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Select 
                                            // options={this.state.opt_rw} onChange={this.handleRW} value={this.state.rw} isDisabled={this.state.dis_rw} 
                                            isClearable />
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Label>RT : </Label>
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Select 
                                            // options={this.state.opt_rt} onChange={this.handleRT} value={this.state.rt} isDisabled={this.state.dis_rt}
                                             isClearable />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Label>Nomor Rumah</Label>
                                        </Col>
                                        <Col md="3" xs="12" style={{ marginTop: '10px' }}>
                                            <Input type="number" id="text-input" name="bdkiWil_rumah" onChange={this.bdkiChange} 
                                            // value={this.state.bdkiWil_rumah} 
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="4"></Col>
                                        <Col xs="6" md="2" lg="2" className="my-2" style={{ marginTop: '10px' }}>
                                            <Button className="btn btn-info btnFilter" 
                                            // onClick={e => { this.saveBdki(e) }}
                                            > Simpan</Button>
                                        </Col>
                                        <Col xs="6" md="2" lg="2" className="my-2" style={{ marginTop: '10px' }}>
                                            <Button className="btn btn-danger btnFilter" onClick={this.toggleNested}> Reset</Button>
                                        </Col>
                                        <Col md="4"></Col>
                                    </Row>
                                </FormGroup>
                            </Col>
                        </ModalBody>
                    </Modal>
                </Row>
            </div>
        )
    }
}

export default TambahDataNonBdki