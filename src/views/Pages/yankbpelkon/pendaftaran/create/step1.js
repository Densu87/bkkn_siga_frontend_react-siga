import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {CardBody, Col, Row, FormGroup, Label, Input, TabPane, TabContent, Button } from 'reactstrap';
import Select from 'react-select';
import Swal from 'sweetalert2';
import BlockUi from 'react-block-ui';
import API013 from '../../../../../services/API013';

class Step1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            blocking: false,
            datas: [],
            noRegisterFaskesKb: '',
            hidden_noRegisterFaskesKb: false,
            noJaringanJejaring: '',
            noJaringanFaskesKB_RO: true,
            namaTempatKB_RO: true,
            jenisFaskesKb: [
                {value: '1', label: 'STATIS'},
                {value: '2', label: 'BERGERAK'},
            ],
            select1: 0,
            show_jenisFaskesKb: [],
            hidden_jenisFaskesKb: true,
            tingkatFaskesKb : [
                {value: '11', label: 'FKRTL', kd_jenisFaskesKb: '1' },
                {value: '12', label: 'FKTP', kd_jenisFaskesKb: '1' },
                {value: '13', label: 'JARINGAN', kd_jenisFaskesKb: '2' },
                {value: '14', label: 'JEJARING', kd_jenisFaskesKb: '2' },
                {value: '15', label: 'MUYAN', kd_jenisFaskesKb: '3'},
                {value: '16', label: 'KAPAL LAUT', kd_jenisFaskesKb: '3'},
            ],
            select2: null,
            show_tingkatFaskesKb: [],
            hidden_tingkatFaskesKb: true,
            tingkatPelayanan: [
                {value: '21', label: 'KLINIK UTAMA', kd_tingkatFaskesKb: '11' },
                {value: '22', label: 'RS UMUM', kd_tingkatFaskesKb: '11'},
                {value: '23', label: 'RS KHUSUS', kd_tingkatFaskesKb: '11'},
                {value: '24', label: 'PUSKESMAS', kd_tingkatFaskesKb: '12'},
                {value: '25', label: 'PRAKTIK DOKTER', kd_tingkatFaskesKb: '12'},
                {value: '26', label: 'KLINIK PRATAMA', kd_tingkatFaskesKb: '12'},
                {value: '27', label: 'RS TIPE D PRATAMA', kd_tingkatFaskesKb: '12'},
                {value: '28', label: 'PUSTU', kd_tingkatFaskesKb: '13'},
                {value: '29', label: 'PUSLING', kd_tingkatFaskesKb: '13'},
                {value: '30', label: 'POSKESDES/POLINDES', kd_tingkatFaskesKb: '13'},
                {value: '31', label: 'PRAKTEK BIDAN', kd_tingkatFaskesKb: '14'},
            ],
            select3: null,
            show_tingkatPelayanan: [],
            hidden_tingkatPelayanan: true,
            select4: null,
            statusKepemilikan : [],
            hidden_statusKepemilikan: true,
            show_kbPerusahaan: [
                { value: 1, label: 'Ya' }, 
                { value: 2, label: 'TIDAK' 
            }],

            kbPerusahaan: 0,
            hidden_kbPerusahaan: true,
            show_pkbrs: [
                { value: 1, label: 'Ya' },
                { value: 2, label: 'TIDAK' 
            }],
            pkbrs: 0,
            hidden_pkbrs: true,

            kerjasamaBpjs: [
                { value: 1, label: 'YA' },
                { value: 2, label: 'TIDAK' }
            ],
            kerjasamaBpjsLnsgTdk: [
                { id: 1, label: 'YA' },
                { id: 2, label: 'TIDAK' }
            ],

            selKerjasamaBpjs: true,
            valKerjasamaBpjs: null,
            selKerjasamaBpjsLsngTdk: true,
            valKerjasamaBpjsLsngTdk: null,
            activeTab: '',  
            namaTempatPelayananKB:'',
            jalan:'',

            jenisTempatLayananId: 0,
            jenisLayananBergerakId: 0,

            jenisFKRTLId: 0,
            jenisFKTPId: 0,
            jenisJaringanId: 0,
            jenisJejaringId: 0,

            KbPerusahaanId: 0,
            pKBRSId: 0,

            kerjasamaBPJSKesehatanId: 0,
            kerjasamaBPJSKesehatanLangsungId: 0,

            nomorRegisterPKSLangsung:'',
            nomorPKSLangsung:'',
            masaBerlakuLangsung: null,
            masaBerlakuLangsungAkhir:null,

            noPksTidakLangsung:'' ,
            masaBerlakuTidakLangsung: null,
            masaBerlakuTidakLangsungAkhir: null,

            fields: {},
            errors: {}

        }
    }

    checkNoReg = (noreg) => {
        var x = 0;
        // console.log(noreg, 'moreg');
        if(this.state.datas.length > 0) {
            this.state.datas.forEach(data => {
                // console.log(data.nomorRegisterFaskesKB)
                if(noreg === data.nomorRegisterFaskesKB){
                    x++;
                }
            });
            // console.log(x, 'tes x');
            if(x > 0){
                this.setState({
                    noJaringanFaskesKB_RO: false, 
                    noJaringanJejaring: '', 
                    namaTempatKB_RO: true,
                });
                ReactDOM.findDOMNode(this.Text_noJaringanJejaring).focus();
            }else{
                Swal.fire({
                    title: 'Info',
                    icon: 'info',
                    text: 'Faskes KB induk belum terdaftar, lanjutkan jika anda ingin mendaftarkan Faskes KB Induk baru.',
                }).then((result) => {
                    Swal.close();
                        this.setState({noJaringanFaskesKB_RO: true, noJaringanJejaring: '', namaTempatKB_RO: false, namaTempatPelayananKB: '', 
                            show_jenisFaskesKb: this.state.jenisFaskesKb.filter(item => item.value === '1'),
                            hidden_jenisFaskesKb: false, select1: null, select2: null, select3: null, select4: null,
                            show_tingkatFaskesKb: [], show_tingkatPelayanan: [], statusKepemilikan : [],
                            hidden_tingkatFaskesKb: true, hidden_tingkatPelayanan: true, hidden_statusKepemilikan: true,
                            kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, 
                            valKerjasamaBpjsLsngTdk: null, selKerjasamaBpjs: true, activeTab: ''
                        });
                    // setTimeout(function () { 
                        ReactDOM.findDOMNode(this.Text_namaTempatKB).focus();
                    // }, 0); 
                });
            }
        }else{
            Swal.fire({
                title: 'Info',
                icon: 'info',
                text: 'Faskes KB induk belum terdaftar, lanjutkan jika anda ingin mendaftarkan Faskes KB Induk baru.',
            }).then((result) => {
                Swal.close();
                    this.setState({noJaringanFaskesKB_RO: true, noJaringanJejaring: '', namaTempatKB_RO: false, namaTempatPelayananKB: '', 
                        show_jenisFaskesKb: this.state.jenisFaskesKb.filter(item => item.value === '1'),
                        hidden_jenisFaskesKb: false, select1: null, select2: null, select3: null, select4: null,
                        show_tingkatFaskesKb: [], show_tingkatPelayanan: [], statusKepemilikan : [],
                        hidden_tingkatFaskesKb: true, hidden_tingkatPelayanan: true, hidden_statusKepemilikan: true,
                        kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, 
                        valKerjasamaBpjsLsngTdk: null, selKerjasamaBpjs: true, activeTab: ''
                    });
                // setTimeout(function () { 
                    ReactDOM.findDOMNode(this.Text_namaTempatKB).focus();
                // }, 0); 
            });
        }
    }

    checkNoJejaring = (noJejaring) => {
        var x = 0;
        if(this.state.datas.length > 0) {
            this.state.datas.forEach(data => {
                if(this.state.noRegisterFaskesKB === data.nomorRegisterFaskesKB && noJejaring === data.nomorJaringanJejaringFaskesKB){
                    x++;
                }
            });
            if(x > 0){
                Swal.fire({
                    title: 'Peringatan',
                    icon: 'warning',
                    text: 'Jaringan Faskes KB sudah terdaftar.',
                });
            }else{
                this.setState({
                    namaTempatKB_RO: false, namaTempatPelayananKB: '',
                    show_jenisFaskesKb: this.state.jenisFaskesKb.filter(item => item.value === '1'),
                    hidden_jenisFaskesKb: false, select1: null, select2: null, select3: null, select4: null,
                    show_tingkatFaskesKb: [], show_tingkatPelayanan: [], statusKepemilikan : [],
                    hidden_tingkatFaskesKb: true, hidden_tingkatPelayanan: true, hidden_statusKepemilikan: true,
                    kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, 
                    valKerjasamaBpjsLsngTdk: null, selKerjasamaBpjs: true, activeTab: ''
                });
                ReactDOM.findDOMNode(this.Text_namaTempatKB).focus();
            }
        }else{
            this.setState({
                namaTempatKB_RO: false, namaTempatPelayananKB: '',
                show_jenisFaskesKb: this.state.jenisFaskesKb.filter(item => item.value === '1'),
                hidden_jenisFaskesKb: false, select1: null, select2: null, select3: null, select4: null,
                show_tingkatFaskesKb: [], show_tingkatPelayanan: [], statusKepemilikan : [],
                hidden_tingkatFaskesKb: true, hidden_tingkatPelayanan: true, hidden_statusKepemilikan: true,
                kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, 
                valKerjasamaBpjsLsngTdk: null, selKerjasamaBpjs: true, activeTab: ''
            });
            ReactDOM.findDOMNode(this.Text_namaTempatKB).focus();
        }
    }
    
    componentDidMount() {
        this.setState({ blocking: true });
        this.setState({ datas: [] });
        API013.get('siga/pelayanankb/getlistbylocation?provinsiId=' + sessionStorage.getItem("kd_prov") + '&kabupatenId=' + sessionStorage.getItem("kd_kab") + '&kecamatanId=0&kelurahanId=0&rwId=0&rtId=0')
        .then(res => {
            if(res.status === 200){ 
                this.setState({ datas: this.state.datas.concat(res.data) });
            }
            this.setState({ blocking: false });
        }).catch((error) => {
            this.setState({ blocking: false });
            Swal.fire({
                title: 'Error',
                icon: 'error',
                text: 'Please Check Your Network Connection.',
            });
        });
    }

    // input nomor faskes kb
    inputFaskeskb = (e) => {
        if(e.target.value){
            // e.target.value = Math.max(0, parseInt(e.target.value) ).toString().slice(0,3)
            this.setState({
                noRegisterFaskesKb: e.target.value,
                show_jenisFaskesKb: this.state.jenisFaskesKb.filter(item => item.value === '1'),
                hidden_jenisFaskesKb: false, select1: null, select2: null, select3: null, select4: null,
                show_tingkatFaskesKb: [], show_tingkatPelayanan: [], statusKepemilikan : [],
                hidden_tingkatFaskesKb: true, hidden_tingkatPelayanan: true, hidden_statusKepemilikan: true,
                kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, 
                valKerjasamaBpjsLsngTdk: null, selKerjasamaBpjs: true, activeTab: ''
            })
        }
        else{
            this.setState({
                show_jenisFaskesKb: [], select1: null, hidden_jenisFaskesKb: true, 
                show_tingkatFaskesKb: [], select2: null, hidden_tingkatFaskesKb: true,
                show_tingkatPelayanan: [], select3: null, hidden_tingkatPelayanan: true,
                select4: null, statusKepemilikan : [], hidden_statusKepemilikan: true, kbPerusahaan: null, pkbrs: null,
                kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, 
                valKerjasamaBpjsLsngTdk: null, selKerjasamaBpjs: true, activeTab: ''
            })
        }
    }

    handleChange = (event) => {
        const state = this.state
		state[event.target.name] = event.target.value
		this.setState(state);
    }

    // Jenis Faskes KB
    changeSel1 = (e) => {
        // console.log(e,'tes e');
        if(e){
            if(e.label === 'STATIS'){
                this.setState({jenisTempatLayananId: 1, jenisLayananBergerakId: 0})
            }else {
                this.setState({jenisTempatLayananId: 0, jenisLayananBergerakId: 1})
            }
            if(this.state.noRegisterFaskesKB){
                if(this.state.noJaringanJejaring){
                    this.setState({
                        show_tingkatFaskesKb: this.state.tingkatFaskesKb.filter(item => item.kd_jenisFaskesKb === '2'),
                        hidden_tingkatFaskesKb: false, hidden_tingkatPelayanan: true, select1: e, select2: null, select3: null,
                    })
                }
                else {
                    this.setState({
                        show_tingkatFaskesKb: this.state.tingkatFaskesKb.filter(item => item.kd_jenisFaskesKb === '1'),
                        hidden_tingkatFaskesKb: false, hidden_tingkatPelayanan: true, select1: e, select2: null, select3: null
                    })
                }
            }
            
        } else {
            this.setState({
                show_tingkatFaskesKb: [], hidden_tingkatFaskesKb: true, hidden_tingkatPelayanan: true, select1: null, select2: null, select3: null,
                select4: null, hidden_statusKepemilikan: true, kbPerusahaan: null, pkbrs: null,
                kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, 
                valKerjasamaBpjsLsngTdk: null, selKerjasamaBpjs: true, activeTab: '', hidden_pkbrs: true, hidden_kbPerusahaan: true,
            })
        }
    }

    // Jenis tinggkat faskes kb
    changeSel2 = (e) => {
        // console.log(e, 'tes ee');
        // this.setState({selectTingkatFaskesKb: e.label})
        if (e) {
            if(e.kd_jenisFaskesKb === '1' || e.kd_jenisFaskesKb === '2'){
                this.setState({
                    show_tingkatPelayanan: this.state.tingkatPelayanan.filter(item => item.kd_tingkatFaskesKb === e.value),
                    hidden_tingkatPelayanan: false, select2: e, select3: null
                })
            } else {
                this.setState({
                    show_tingkatPelayanan: [], select2: e, select3: null, select4: null, hidden_statusKepemilikan: true,
                    kbPerusahaan: null, pkbrs: null, 
                })   
            }
        }
        else {
            this.setState({
                show_tingkatPelayanan: [], select2: null, hidden_tingkatPelayanan: true, select3: null, select4: null, 
                hidden_statusKepemilikan: true, kbPerusahaan: null, pkbrs: null,
                kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, 
                valKerjasamaBpjsLsngTdk: null, selKerjasamaBpjs: true, activeTab: '', hidden_pkbrs: true, hidden_kbPerusahaan: true,
            })
        }
    }

    // Jenis tingkat pelayan
    changeSel3 = (e) => {
        console.log(e,'tes e 3');
        if(e){
            if(e.value === '21' ){
                this.setState({
                    statusKepemilikan: [{value: '8', label: 'BUMN/BUMD'}, {value: '9', label: 'SWASTA'},{value: '10', label: 'LSOM'},],
                    select3: e,  hidden_statusKepemilikan: false,
                    jenisFKRTLId: 1, jenisFKTPId: 0, jenisJaringanId: 0, jenisJejaringId: 0
                })
            }
            else if(e.value === '22' || e.value === '23'){
                (e.value === '22') ? this.setState({jenisFKRTLId: 2}) : this.setState({jenisFKRTLId: 3})
                
                this.setState({
                    statusKepemilikan: [{value: '1', label: 'KEMENKES PUSAT'}, {value: '2', label: 'DINKES PROVINSI'},
                                        {value: '3', label: 'DINKES KAB/KOTA'}, {value: '6', label: 'TNI'}, {value: '7', label: 'POLRI'},
                                        {value: '8', label: 'BUMN/BUMD'}, {value: '9', label: 'SWASTA'}, {value: '10', label: 'LSOM'},],
                    select3: e, hidden_statusKepemilikan: false, 
                    jenisFKTPId: 0, jenisJaringanId: 0, jenisJejaringId: 0
                })
            }
            else if (e.value === '24') {
                this.setState({
                    statusKepemilikan: [{value: '3', label: 'DINKES KABUPATEN/KOTA'}], select3: e, hidden_statusKepemilikan: false,
                    jenisFKRTLId: 0, jenisFKTPId: 4, jenisJaringanId: 0, jenisJejaringId: 0
                })
            }
            else if (e.value === '25'){
                this.setState({
                    statusKepemilikan: [{value: '9', label: 'SWASTA'}], select3: e, hidden_statusKepemilikan: false,
                    jenisFKRTLId: 0, jenisFKTPId: 5, jenisJaringanId: 0, jenisJejaringId: 0
                })
            }
            else if (e.value === '26'){
                this.setState({
                    statusKepemilikan: [{value: '1', label: 'KEMENKES PUSAT'}, {value: '2', label: 'DINKES PROVINSI'}, 
                                        {value: '3', label: 'DINKES KAB/KOTA'},{value: '6', label: 'TNI'}, {value: '7', label: 'POLRI'}, 
                                        {value: '8', label: 'BUMN/BUMD'}, {value: '9', label: 'SWASTA'}, {value: '10', label: 'LSOM'},],
                    select3: e, hidden_statusKepemilikan: false,
                    jenisFKRTLId: 0, jenisFKTPId: 6, jenisJaringanId: 0, jenisJejaringId: 0
                })
                
            }
            else if (e.value === '27'){
                this.setState({
                    statusKepemilikan: [{value: '1', label: 'KEMENKES PUSAT'}, {value: '2', label: 'DINKES PROVINSI'}, 
                                        {value: '3', label: 'DINKES KAB/KOTA'},{value: '6', label: 'TNI'}, {value: '7', label: 'POLRI'}, 
                                        {value: '8', label: 'BUMN/BUMD'}, {value: '9', label: 'SWASTA'}, {value: '10', label: 'LSOM'},],
                    select3: e, hidden_statusKepemilikan: false,
                    jenisFKRTLId: 0, jenisFKTPId: 7, jenisJaringanId: 0, jenisJejaringId: 0
                })
            }
            else if (e.value === '28' || e.value === '29' || e.value === '30') {
                if(e.value === '28'){
                    this.setState({jenisJaringanId: 1})
                }else if (e.value === '29') {
                    this.setState({jenisJaringanId: 2})
                }else {
                    this.setState({jenisJaringanId: 3})
                }
                this.setState({
                    statusKepemilikan: [{value: '3', label: 'DINKES KAB/KOTA'}], select3: e, hidden_statusKepemilikan: false,
                    jenisFKRTLId: 0, jenisFKTPId: 0, jenisJejaringId: 0
                })
            }
            else if (e.value === '31') {
                this.setState({
                    statusKepemilikan: [{value: '9', label: 'SWASTA'}], select3: e, hidden_statusKepemilikan: false,
                    jenisFKRTLId: 0, jenisFKTPId: 0, jenisJaringanId: 0, jenisJejaringId: 5
                })
            }
        }
        else {
            this.setState({
                statusKepemilikan: [], select3: null, select4: null, hidden_statusKepemilikan: true, kbPerusahaan: null, pkbrs: null,
                kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, 
                valKerjasamaBpjsLsngTdk: null, selKerjasamaBpjs: true, activeTab: '', hidden_pkbrs: true, hidden_kbPerusahaan: true,
                jenisFKRTLId: 0, jenisFKTPId: 0, jenisJaringanId: 0, jenisJejaringId: 0
            })
        }
    }

    changeSel4 = (e) => {
        if(e){
            if(this.state.select3.value === '21'){
                this.setState({
                    select4: e, kbPerusahaan: [{ value: '2', label: 'TIDAK' }], pkbrs: [{ value: '2', label: 'TIDAK' }],
                    selKerjasamaBpjs: false
                })
            }
            else if (this.state.select3.value === '22' || this.state.select3.value === '23'){
                this.setState({
                    select4: e, 
                    kbPerusahaan: [{ value: '2', label: 'TIDAK' }], 
                    // pkbrs: [{ value: 'ya', label: 'Ya' }],
                    hidden_pkbrs: false,
                    // selKerjasamaBpjs: false
                })
            }
            else if(this.state.select3.value === '24'){
                this.setState({
                    select4: e, kbPerusahaan: [{ value: '2', label: 'TIDAK' }], pkbrs: [{ value: '2', label: 'TIDAK' }],
                    selKerjasamaBpjs: false
                })
            }
            else if(this.state.select3.value === '25'){
                this.setState({
                    select4: e, kbPerusahaan: [{ value: '2', label: 'TIDAK' }], pkbrs: [{ value: '2', label: 'TIDAK' }],
                    selKerjasamaBpjs: false
                })
            }
            else if(this.state.select3.value === '26'){
                // if(e.label === 'KEMENKES PUSAT' || e.label === 'DINKES PROVINSI' || e.label === 'DINKES KAB/KOTA' || e.label === 'TNI' || e.label === 'POLRI'){
                    this.setState({
                        select4: e, 
                        // kbPerusahaan: [{ value: 'tidak', label: 'TIDAK' }], 
                        pkbrs: [{ value: '2', label: 'TIDAK' }],
                        hidden_kbPerusahaan: false,
                    })
                // }
                // else if(e.label === 'BUMN/BUMD' || e.label === 'SWASTA' || e.label === 'LSOM'){
                //     this.setState({
                //         select4: e, kbPerusahaan: [{ value: 'ya', label: 'Ya' }], pkbrs: [{ value: 'tidak', label: 'TIDAK' }],
                //         selKerjasamaBpjs: false
                //     })
                // }
            }
            else if(this.state.select3.value === '27'){
                this.setState({
                    select4: e, 
                    kbPerusahaan: [{ value: '2', label: 'TIDAK' }], 
                    // pkbrs: [{ value: 'ya', label: 'Ya' }],
                    hidden_pkbrs: false,
                })
            } 
            else if(this.state.select3.value === '28' || this.state.select3.value === '29' || this.state.select3.value === '30') {
                this.setState({
                    select4: e, kbPerusahaan: [{ value: '2', label: 'TIDAK' }], pkbrs: [{ value: '2', label: 'TIDAK' }],
                    selKerjasamaBpjs: false
                })
            }
            else if(this.state.select3.value === '31'){
                this.setState({
                    select4: e, kbPerusahaan: [{ value: '2', label: 'TIDAK' }], pkbrs: [{ value: '2', label: 'TIDAK' }],
                    selKerjasamaBpjs: false
                })
            }
        }
        else {
            this.setState({
                select4: null, kbPerusahaan: null, pkbrs: null, 
                kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, 
                valKerjasamaBpjsLsngTdk: null, selKerjasamaBpjs: true, hidden_pkbrs: true, hidden_kbPerusahaan: true, activeTab: ''
            })
        }
    }

    // selKerjasamaBpjs: true,
    // selKerjasamaBpjsLsngTdk: true,

    changeKbPerusahaan = (e) => {
        console.log(e, 'tes e');
        if(e){
            if(e.label === 'TIDAK'){
                this.setState({KbPerusahaanId: 2})
            } else {
                this.setState({KbPerusahaanId: 1})
            }

            if(this.state.select3.value === '26'){
                this.setState({
                    kbPerusahaan: e, selKerjasamaBpjs: false
                })
            }
        }
        else {
            this.setState({
                kbPerusahaan: [], selKerjasamaBpjs: true
            })
        }
    }

    changeSelPkbrs = (e) => {  
        console.log(e, 'tes e2');
        if(e){
            if(e.value === '2'){
                this.setState({pKBRSId: 2})
            } else {
                this.setState({pKBRSId: 1})
            }

            if(this.state.select3.value === '22' || this.state.select3.value === '23') {
                this.setState({
                    pkbrs: e, selKerjasamaBpjs: false
                })
            }
            else if(this.state.select3.value === '27') {
                this.setState({
                    pkbrs: e, selKerjasamaBpjs: false
                })
            }
        }
        else{
            this.setState({
                pkbrs: [], selKerjasamaBpjs: true, selKerjasamaBpjsLsngTdk: true, kerjasamaBpjsLsngTdk: [],
                valKerjasamaBpjs: null, valKerjasamaBpjsLsngTdk: null, activeTab: ''
            })
        }
        // console.log(e)
    }

    option4 = (e) => {
        console.log(e, 'tes e 4')
        if(e){
            if(e.value === 1){
                this.setState({
                    kerjasamaBpjsLsngTdk: [{ value: '2', label: 'Tidak Langsung' }], selKerjasamaBpjsLsngTdk: false, valKerjasamaBpjs: e, kerjasamaBPJSKesehatanId: 1
                })
                
                // if(this.state.noJaringanJejaring){
                //     this.setState({
                //         kerjasamaBpjsLsngTdk: [{ value: '2', label: 'Tidak Langsung' }], selKerjasamaBpjsLsngTdk: false, valKerjasamaBpjs: e
                //     })
                // }
                // else if (this.state.noRegisterFaskesKb) {
                //     this.setState({
                //         kerjasamaBpjsLsngTdk: [{ value: '1', label: 'Langsung' }], selKerjasamaBpjsLsngTdk: false, valKerjasamaBpjs: e
                //     })
                // }
            }
            else if(e.value === 2) {
                this.setState({
                    kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: e, valKerjasamaBpjsLsngTdk: null, activeTab: '', kerjasamaBPJSKesehatanId: 2, 
                })
            }
        }
        else{
            this.setState({
                kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, valKerjasamaBpjsLsngTdk: null, activeTab: '', kerjasamaBPJSKesehatanId: 0
            })
        }
    }

    option5 = (e) => {
        if (e === null){
            this.setState({
                activeTab: '', valKerjasamaBpjsLsngTdk: null, kerjasamaBPJSKesehatanLangsungId: 0
            })
        } else if (e.value === '1'){
            this.setState({
                activeTab: '1', valKerjasamaBpjsLsngTdk: e, kerjasamaBPJSKesehatanLangsungId: 1
            })
        } else if (e.value === '2'){
            this.setState({
                activeTab: '2', valKerjasamaBpjsLsngTdk: e, kerjasamaBPJSKesehatanLangsungId: 2
            })
        }
    }

    convertDate = (dt) => {
        console.log(dt, 'tes dt');
        var curr = new Date(dt);
        // curr.setDate(dt);
        return curr.toISOString();
    }

    handleValidation(){
        let state = this.state;
        let errors = {};
        let formIsValid = true;

        //Jalan
        if(!state.jalan){
           formIsValid = false;
           errors["jalan"] = "Tidak Boleh Kosong";
        }

        //Nama tempat pelayanan KB
        if(!state.namaTempatPelayananKB){
            formIsValid = false;
            errors["namaTempatPelayananKB"] = "Tidak Boleh Kosong";
         }

       this.setState({errors: errors});
       return formIsValid;
   }

    handleNext = (event) => {
        //validasi
        event.preventDefault();

        if(this.handleValidation()){
            // console.log(sessionStorage.getItem('kd_prov')+ 'tes');
            const tpkb =  sessionStorage.getItem('depdagriProv')+sessionStorage.getItem('depdagriKab')+this.state.noRegisterFaskesKB+this.state.noJaringanJejaring
            sessionStorage.setItem('tempatPelayananKBId', tpkb)

            const step1 = {
                tempatPelayananKBId: tpkb,
                provinsiId:  parseInt(sessionStorage.getItem('kd_prov')), 
                kabupatenId: parseInt(sessionStorage.getItem('kd_kab')),
                kecamatanId: parseInt(sessionStorage.getItem('kd_kec')),
                kelurahanId: parseInt(sessionStorage.getItem('kd_des')),
                rwId: parseInt(sessionStorage.getItem('kd_rw')),
                rtId: parseInt(sessionStorage.getItem('kd_rt')),
                nomorRegisterFaskesKB: this.state.noRegisterFaskesKB,
                nomorJaringanJejaringFaskesKB: this.state.noJaringanJejaring,
                namaTempatPelayananKB: this.state.namaTempatPelayananKB,
                alamatJalan: this.state.jalan,

                jenisFKRTLId: this.state.jenisFKRTLId,
                jenisFKTPId: this.state.jenisFKTPId,
                jenisJaringanId: this.state.jenisJaringanId,
                jenisJejaringId: this.state.jenisJaringanId,
                jenisTempatLayananId: this.state.jenisTempatLayananId,
                jenisLayananBergerakId: this.state.jenisLayananBergerakId,
                kepemilikanId:  parseInt((this.state.select4) ? this.state.select4.value:''),
                kBPerusahaanId: this.state.KbPerusahaanId,
                pKBRSId: this.state.pKBRSId,

                kerjasamaBPJSKesehatanId: this.state.kerjasamaBPJSKesehatanId,
                kerjasamaBPJSKesehatanLangsungId:this.state.kerjasamaBPJSKesehatanLangsungId,

                nomorPKSLangsung: this.state.nomorPKSLangsung,
                masaBerlakuPKSLangsung: this.state.masaBerlakuLangsung,
                masaBerlakuPKSLangsungAkhir: this.state.masaBerlakuLangsungAkhir,
                nomorRegisterPKSLangsung: this.state.nomorRegisterPKSLangsung,

                nomorPKSTidakLangsung: this.state.noPksTidakLangsung,
                masaBerlakuPKSTidakLangsung: this.convertDate(this.state.masaBerlakuTidakLangsung),
                masaBerlakuPKSTidakLangsungAkhir: this.convertDate(this.state.masaBerlakuTidakLangsungAkhir),
            };
            console.log(step1, 'tes')
            this.props.handleValueStep('step1', step1)
            this.props.nextStep();
         } else{
            Swal.fire({  
                title: 'Peringatan',  
                icon: 'warning',  
                text: 'Form nya masih kosong! silahkan diisi dulu yaa :)',  
            });
         }
        
    }

    noRegisterFaskesKBChange = (e) => {
        this.setState({noRegisterFaskesKB: e.target.value.replace(/\D/,'')},
        ()=>{
            if(this.state.noRegisterFaskesKB.length === 3) {
                // this.checkNoReg(sessionStorage.getItem("kd_prov"), sessionStorage.getItem("kd_kab"), sessionStorage.getItem("kd_kec"), sessionStorage.getItem("kd_des"), sessionStorage.getItem("kd_rt"), sessionStorage.getItem("kd_rw"))
                // this.checkNoReg(sessionStorage.getItem("kd_prov"), sessionStorage.getItem("kd_kab"), sessionStorage.getItem("kd_kec"), 0, 0, 0)
                this.checkNoReg(this.state.noRegisterFaskesKB);
            }
        })
    }

    noJaringanJejaringChange = (e) => {
        this.setState({noJaringanJejaring: e.target.value.replace(/\D/,'')},
        ()=>{
            if(this.state.noJaringanJejaring.length === 2) {
                this.checkNoJejaring(this.state.noJaringanJejaring);
            }
            // else{
            //     this.setState({
            //         noJaringanJejaring: '',
            //         select1: null, show_tingkatFaskesKb: [], select2: null, hidden_tingkatFaskesKb: true,
            //         show_tingkatPelayanan: [], statusKepemilikan : [], select3: null, hidden_tingkatPelayanan: true, select4: null,
            //         hidden_statusKepemilikan: true, kbPerusahaan: null, pkbrs: null,
            //         kerjasamaBpjsLsngTdk: [], selKerjasamaBpjsLsngTdk: true, valKerjasamaBpjs: null, 
            //         valKerjasamaBpjsLsngTdk: null, selKerjasamaBpjs: true, activeTab: ''
            //     })
            // }
        })
    }

    render() {
        var curr = new Date();
        curr.setDate(curr.getDate());
        var dateNow = curr.toISOString().substr(0, 10);

        return (
            <div>
                <BlockUi tag="div" blocking={this.state.blocking}>
                    <Row>
                        <Col sm="12">
                            <Row>
                                <Col md="2"></Col>
                                
                                <Col md="8" style={{ textAlign: 'center' }}>
                                <CardBody>
                                    <h6>KARTU PENDAFTARAN TEMPAT PELAYANAN KB</h6>
                                    <Row>
                                        <Col md="3" xs="6" style={{ textAlign: 'center' }} >
                                            <Row>
                                                <Input style={{ marginRight: '5px', textAlign: 'center' }} type="read-only" id="kodeProvinsi" name="kodeProvinsi" value={sessionStorage.getItem("depdagriProv")} disabled/>
                                            </Row>
                                            <Row >
                                                <Col md="12" style={{ textAlign: 'center' }}>
                                                    <Label>Kode Provinsi</Label>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md="3" xs="6">
                                            <Row>
                                                <Input style={{ marginRight: '5px', textAlign: 'center' }} type="read-only" id="kodeKabupatenKota" name="kodeKabupatenKota" value={sessionStorage.getItem("depdagriKab")} disabled/>
                                            </Row>
                                            <Row>
                                                <Col md="12" style={{ textAlign: 'center' }}>
                                                    <Label>Kode Kabupaten/Kota</Label>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md="3" xs="6">
                                            <Row>
                                                <Input type="text" 
                                                onChange={this.noRegisterFaskesKBChange}
                                                maxLength="3"
                                                disabled={this.state.hidden_noRegisterFaskesKb}
                                                style={{ marginRight: '5px', textAlign: 'center' }} 
                                                value={this.state.noRegisterFaskesKB || ''} />
                                            </Row>
                                            <Row>
                                                <Col md="12" style={{ textAlign: 'center' }}>
                                                    <Label>No. Register Faskes KB</Label>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md="3" xs="6">
                                            <Row>
                                                <Input type="text" 
                                                readOnly={this.state.noJaringanFaskesKB_RO}
                                                ref={c => (this.Text_noJaringanJejaring = c)}
                                                value={this.state.noJaringanJejaring}
                                                maxLength="2"
                                                // onInput={this.JaringanJejaring}
                                                onChange={this.noJaringanJejaringChange}
                                                style={{ marginRight: '5px', textAlign: 'center' }} />
                                            </Row>
                                            <Row>
                                                <Col md="12" style={{ textAlign: 'center' }}>
                                                    <Label>No. Jaringan/Jejaring Faskes KB</Label>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </CardBody>
                                </Col>
                                <Col md="2"></Col>
                            </Row>
                            <CardBody className="card-body-nopad">
                            <h6>I. Identitas </h6>
                            <div style={{position:'absolute', right: '20px', marginTop:'-30px', fontSize:'12px'}}>{this.props.currentStep}/{this.props.totalSteps}</div>
                                <FormGroup>
                                    <Row>
                                        <Col md="4">
                                            <Label className="labelForm">1. Nama Tempat Pelayan KB</Label>
                                        </Col>
                                        <Col md="4">
                                            <Input type="text" readOnly={this.state.namaTempatKB_RO} value={this.state.namaTempatPelayananKB} ref={c => (this.Text_namaTempatKB = c)} name="namaTempatPelayananKB" onChange={this.handleChange}/>
                                            <span style={{color: "red"}}>{this.state.errors["namaTempatPelayananKB"]}</span>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="12">
                                            <Label style={{ marginTop: '10px' }} className="labelForm">2. Alamat</Label>
                                            <Row>
                                                <Col md="4" xs="12" style={{ paddingTop: '10px' }}>
                                                    <Label className="labelForm">a. Jalan :</Label>
                                                </Col>
                                                <Col md="4" style={{ paddingTop: '10px' }}>
                                                    <Input type="text" readOnly={this.state.namaTempatKB_RO} id="jalan" name="jalan" value={this.state.jalan} onChange={this.handleChange} />
                                                    <span style={{color: "red"}}>{this.state.errors["jalan"]}</span>
                                                </Col>
                                                <Col md="2" xs="6" style={{ paddingTop: '10px' }}>
                                                    <Row>
                                                        <Col md="3" xs="4"><Label className="labelForm">RT</Label></Col>
                                                        <Col md="9" xs="8"><Input type="read-only" id="rt" name="rt" value={sessionStorage.getItem("rt")} /></Col>
                                                    </Row>
                                                </Col>
                                                <Col md="2" xs="6" style={{ paddingTop: '10px' }}>
                                                    <Row>
                                                        <Col md="3" xs="4"><Label className="labelForm">RW</Label></Col>
                                                        <Col md="9" xs="8"><Input type="read-only" id="rw" name="rw" value={sessionStorage.getItem("rw")} /></Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md="4" style={{marginTop:'10px'}}>
                                                    <Label>b. Desa/Kelurahan :</Label>
                                                </Col>
                                                <Col md="7" xs="9" style={{marginTop:'10px'}}>
                                                    <Input type="text" id="desaKelurahan" name="desaKelurahan" value={sessionStorage.getItem("desa")} />
                                                </Col>
                                                <Col md="1" xs="3" style={{marginTop:'10px'}}>
                                                    <Input type="text" id="kodeDesa" name="kodeDesa" value={sessionStorage.getItem("kd_des")} />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md="4" style={{marginTop:'10px'}}>
                                                    <Label>c. Kecamatan :</Label>
                                                </Col>
                                                <Col md="7" xs="9" style={{marginTop:'10px'}}>
                                                    <Input type="text" id="kecamatan" name="kecamatan" value={sessionStorage.getItem("kecamatan")} />
                                                </Col>
                                                <Col md="1" xs="3" style={{marginTop:'10px'}}>
                                                    <Input type="text" id="kodeKecamatan" name="kodeKecamatan" value={sessionStorage.getItem("kd_kec")} />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md="4" style={{marginTop:'10px'}}>
                                                    <Label>d. Kabupaten :</Label>
                                                </Col>
                                                <Col md="7" xs="9" style={{marginTop:'10px'}}>
                                                    <Input type="text" id="kabupaten" name="kabupaten" value={sessionStorage.getItem("kabupaten")} />
                                                </Col>
                                                <Col md="1" xs="3" style={{marginTop:'10px'}}>
                                                    <Input type="text" id="kodeKabupaten" name="kodeKabupaten" value={sessionStorage.getItem("depdagriKab")} />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md="4" style={{marginTop:'10px'}}>
                                                    <Label>e. Provinsi :</Label>
                                                </Col>
                                                <Col md="7" xs="9" style={{marginTop:'10px'}}>
                                                    <Input type="text" id="provinsi" name="provinsi" value={sessionStorage.getItem("provinsi")} />
                                                </Col>
                                                <Col md="1" xs="3" style={{marginTop:'10px'}}>
                                                    <Input type="text" id="kodeProvinsi" name="kodeProvinsi" value={sessionStorage.getItem("depdagriProv")} />
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>

                                    <Row style={{ paddingTop: '20px' }}>
                                        <Col md="4" xs="12" style={{ paddingTop: '10px' }}>
                                            <Label>3. Jenis</Label>
                                        </Col>
                                        <Col md="2" xs="12" style={{ paddingTop: '10px' }}>
                                            <Select id="jenisFaskesKB" name="jenisFaskesKB" options={this.state.show_jenisFaskesKb} onChange={this.changeSel1} placeholder="Jenis Faskes KB" value={this.state.select1} isDisabled={this.state.hidden_jenisFaskesKb} isClearable />
                                        </Col>
                                        <Col md="2" xs="12" style={{ paddingTop: '10px' }}>
                                            <Select id="jenisTingkatFaskesKB" name="jenisTingkatFaskesKB" options={this.state.show_tingkatFaskesKb} onChange={this.changeSel2} placeholder="Jenis Tingkat Faskes KB" value={this.state.select2} isDisabled={this.state.hidden_tingkatFaskesKb} isClearable />
                                        </Col>
                                        <Col md="2" xs="12"style={{ paddingTop: '10px' }}>
                                            <Select id="jenisTingkatPelayanan" name="jenisTingkatPelayanan" options={this.state.show_tingkatPelayanan} onChange={this.changeSel3} placeholder="Jenis Tingkat Pelayanan" value={this.state.select3} isDisabled={this.state.hidden_tingkatPelayanan} isClearable />
                                        </Col>
                                    </Row>
                                    <Row style={{ marginTop: '15px' }}>
                                        <Col md='4' style={{ paddingTop: '10px' }}>
                                            <Label>4. Kepemilikan</Label>
                                        </Col>
                                        <Col md="2" style={{ paddingTop: '10px' }}>
                                            <Select id="statusKepemilikan" name="statusKepemilikan" options={this.state.statusKepemilikan} onChange={this.changeSel4} value={this.state.select4} isDisabled={this.state.hidden_statusKepemilikan} placeholder="Status Kepemilikan" isClearable />
                                        </Col>
                                    </Row>
                                    <Row style={{ marginTop: '15px' }}>
                                        <Col md='4' style={{ paddingTop: '10px' }}>
                                            <Label>5. Apakah faskes KB termasuk pada:</Label>
                                        </Col>
                                        <Col md="1" style={{ paddingTop: '10px' }}>
                                            <Label>KB Perusahaan</Label>
                                        </Col>
                                        <Col md="2" style={{ paddingTop: '10px' }}>
                                            <Select id="kbPerusahaan" name="kbPerusahaan" options={this.state.show_kbPerusahaan} isDisabled={this.state.hidden_kbPerusahaan} onChange={this.changeKbPerusahaan} value={this.state.kbPerusahaan} isClearable />
                                        </Col>
                                        <Col md="1" style={{ paddingTop: '10px' }}>
                                            <Label>PKBRS</Label>
                                        </Col>
                                        <Col md="2" style={{ paddingTop: '10px' }}>
                                            <Select id="pkbrs" name="pkbrs" options={this.state.show_pkbrs} isDisabled={this.state.hidden_pkbrs} onChange={this.changeSelPkbrs} value={this.state.pkbrs} isClearable />
                                        </Col>
                                    </Row>
                                    <Row style={{ marginTop: '15px' }}>
                                        <Col md="4" xs="12" style={{ paddingTop: '10px' }}>
                                            <Label>6. Kerjasama Dengan BPJS Kesehatan </Label>
                                            <Label style={{ paddingLeft: '10px'}}>(pilih Ya atau Tidak, jika Ya, maka pilih Langsung atau Tidak Langsung, selanjutnya isi No. PKS, masa berlaku PKS dan no. registernya pada BPJS Kesehatan)</Label>
                                        </Col>
                                        <Col md="2" xs="6" style={{ paddingTop: '10px' }}>
                                            <Select options={this.state.kerjasamaBpjs} isDisabled={this.state.selKerjasamaBpjs} value={this.state.valKerjasamaBpjs} onChange={this.option4} isClearable />    
                                        </Col>
                                        <Col md="2" xs="6" style={{ paddingTop: '10px' }}>
                                            <Select options={this.state.kerjasamaBpjsLsngTdk} isDisabled={this.state.selKerjasamaBpjsLsngTdk} value={this.state.valKerjasamaBpjsLsngTdk} onChange={this.option5} isClearable />    
                                        </Col>
                                    </Row>

                                    <TabContent style={{ border: 'none' }} activeTab={this.state.activeTab}>
                                        <TabPane tabId="1">
                                            <Row style={{ marginTop: '5px' }}>
                                                <Col md="4"></Col>
                                                <Col md="4">
                                                    <Label>Langsung</Label>
                                                    <Row style={{ marginTop: '15px' }}>
                                                        <Col md="4">
                                                            <Label>No. PKS : </Label>
                                                        </Col>
                                                        <Col md="5">
                                                            <Input type="text" name="noPksLangsung" value={this.state.noPksLangsung} onChange={this.handleChange}/>
                                                        </Col>
                                                    </Row>
                                                    <Row style={{ marginTop: '15px' }}>
                                                        <Col md="4">
                                                            <Label>Masa Berlaku Awal : </Label>
                                                        </Col>
                                                        <Col md="5">
                                                            <Input type="date" name="masaBerlakuLangsung"   onChange={this.handleChange}/>
                                                        </Col>
                                                    </Row>
                                                    <Row style={{ marginTop: '15px' }}>
                                                        <Col md="4">
                                                            <Label>Masa Berlaku Akhir : </Label>
                                                        </Col>
                                                        <Col md="5">
                                                            <Input type="date" name="masaBerlakuLangsungAkhir"   onChange={this.handleChange}/>
                                                        </Col>
                                                    </Row>
                                                    <Row style={{ marginTop: '15px' }}>
                                                        <Col md="4">
                                                            <Label>No. Register : </Label>
                                                        </Col>
                                                        <Col md="5">
                                                        <Input type="text" id="text-input" name="nomorRegisterPKSLangsung" value={this.state.nomorRegisterPKSLangsung} onChange={this.handleChange} />
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </TabPane>

                                        <TabPane tabId="2">
                                            <Row style={{ marginTop: '5px' }}>
                                                <Col md="4"></Col>
                                                <Col md="4">
                                                    <Label>Tidak Langsung</Label>
                                                    <Row style={{ marginTop: '15px' }}>
                                                        <Col md="4">
                                                            <Label>No. PKS : </Label>
                                                        </Col>
                                                        <Col md="7">
                                                            <Input type="text" id="text-input" name="noPksTidakLangsung" value={this.state.noPksTidakLangsung} onChange={this.handleChange} />
                                                        </Col>
                                                    </Row>
                                                    <Row style={{ marginTop: '15px' }}>
                                                        <Col md="4">
                                                            <Label>Masa Berlaku Awal : </Label>
                                                        </Col>
                                                        <Col md="7">
                                                            <Input type="date" id="text-input" name="masaBerlakuTidakLangsung"   onChange={this.handleChange}/>
                                                        </Col>
                                                    </Row>
                                                    <Row style={{ marginTop: '15px' }}>
                                                        <Col md="4">
                                                            <Label>Masa Berlaku Akhir : </Label>
                                                        </Col>
                                                        <Col md="7">
                                                            <Input type="date" id="text-input" name="masaBerlakuTidakLangsungAkhir"   onChange={this.handleChange}/>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </TabPane>
                                    </TabContent>

                                </FormGroup>
                            </CardBody>
                        </Col>
                    </Row>
                    <div style={{display:'flex', justifyContent:'space-between'}}>
                        <Button className="btn btn-warning" onClick={this.props.buttonBack}>Sebelumnya</Button>
                        <Button className="btn btn-info" onClick={this.handleNext}>Selanjutnya</Button>
                    </div>
                </BlockUi>
            </div>
        )
    }
}

export default Step1;